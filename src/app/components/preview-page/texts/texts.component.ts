import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-texts',
  templateUrl: './texts.component.html',
  styleUrls: ['./texts.component.scss'],
})
export class TextsComponent implements OnInit {
  @Input() content: string;
  constructor() {}

  ngOnInit(): void {}
}
