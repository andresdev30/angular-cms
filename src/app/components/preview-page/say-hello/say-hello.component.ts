import {Component, Input, OnInit} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-say-hello',
  templateUrl: './say-hello.component.html',
  styleUrls: ['./say-hello.component.scss']
})
export class SayHelloComponent implements OnInit {
  @Input() userLoggedIn: string;

  currentDate: string;

  constructor() { }

  ngOnInit(): void {
    this.setCurrentDate();
  }

  setCurrentDate(): void {
    this.currentDate = moment().format('MMMM Do YYYY, h:mm:ss a');
  }
}
