import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
})
export class EditPageComponent implements OnInit {
  @Input() images: string[];
  @Input() texts: string;
  @Input() showHelloInput: boolean;
  @Output() sendImages = new EventEmitter();
  @Output() sendText = new EventEmitter();
  @Output() showHello = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  sendingImages(e): void {
    this.sendImages.emit(e);
  }

  sendingText(e): void {
    this.sendText.emit(e);
  }

  showingHello(e): void {
    this.showHello.emit(e);
  }
}
