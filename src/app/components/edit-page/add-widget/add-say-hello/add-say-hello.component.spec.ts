import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSayHelloComponent } from './add-say-hello.component';

describe('AddSayHelloComponent', () => {
  let component: AddSayHelloComponent;
  let fixture: ComponentFixture<AddSayHelloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSayHelloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSayHelloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
