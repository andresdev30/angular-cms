import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-say-hello',
  templateUrl: './add-say-hello.component.html',
  styleUrls: ['./add-say-hello.component.scss'],
})
export class AddSayHelloComponent implements OnInit {
  showHelloForm: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<AddSayHelloComponent>,
    @Inject(MAT_DIALOG_DATA) private data: boolean
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    this.showHelloForm = new FormGroup({
      showHello: new FormControl(this.data),
    });
  }

  onClose(): void {
    if (this.showHelloForm.get('showHello').value !== this.data) {
      this.dialogRef.close(this.showHelloForm.get('showHello').value);
    }
  }
}
