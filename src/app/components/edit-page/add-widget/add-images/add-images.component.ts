import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-images',
  templateUrl: './add-images.component.html',
  styleUrls: ['./add-images.component.scss'],
})
export class AddImagesComponent implements OnInit {
  addImagesForm: FormGroup;
  selectedFile = null;
  images: any[] = [];

  constructor(
    public dialogRef: MatDialogRef<AddImagesComponent>,
    @Inject(MAT_DIALOG_DATA) private data: string[]
  ) {}

  ngOnInit(): void {
    this.createForm();
    if (this.data) {
      this.images = this.data;
    }
  }

  private createForm(): void {
    this.addImagesForm = new FormGroup({
      imageInput: new FormControl(null),
    });
  }

  onImageSelected(e: any): void {
    console.log(typeof e);
    this.selectedFile = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(this.selectedFile);
    reader.addEventListener('load', () => {
      this.images.push(reader.result);
    });
  }

  removeImage(img): void {
    this.images = this.images.filter((i) => i !== img);
  }

  onCloseClick(): void {
    this.dialogRef.close(this.images);
  }
}
