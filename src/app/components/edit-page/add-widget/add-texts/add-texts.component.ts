import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-texts',
  templateUrl: './add-texts.component.html',
  styleUrls: ['./add-texts.component.scss'],
})
export class AddTextsComponent implements OnInit {
  contentForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddTextsComponent>,
    @Inject(MAT_DIALOG_DATA) private data: string
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    this.contentForm = new FormGroup({
      content: new FormControl(this.data),
    });
  }

  onSaveClick(): void {
    if (this.contentForm.get('content').value) {
      this.dialogRef.close(this.contentForm.get('content').value);
    }
  }
}
