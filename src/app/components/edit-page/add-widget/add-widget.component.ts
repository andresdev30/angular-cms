import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddImagesComponent } from './add-images/add-images.component';
import { AddSayHelloComponent } from './add-say-hello/add-say-hello.component';
import { AddTextsComponent } from './add-texts/add-texts.component';

@Component({
  selector: 'app-add-widget',
  templateUrl: './add-widget.component.html',
  styleUrls: ['./add-widget.component.scss'],
})
export class AddWidgetComponent implements OnInit {
  @Input() images: string[];
  @Input() texts: string;
  @Input() showHelloInput: boolean;
  @Output() sendImages = new EventEmitter();
  @Output() sendText = new EventEmitter();
  @Output() showHello = new EventEmitter();
  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  openAddImagesDialog(): void {
    const dialogRef = this.dialog.open(AddImagesComponent, {
      data: this.images ? this.images : null,
    });
    dialogRef.afterClosed().subscribe((images) => {
      if (images !== undefined) {
        this.sendImages.emit(images);
      }
    });
  }

  openAddSayHelloDialog(): void {
    const dialogRef = this.dialog.open(AddSayHelloComponent, {
      data: this.showHelloInput,
    });
    dialogRef.afterClosed().subscribe((show) => {
      if (show !== undefined) {
        this.showHello.emit(show);
      }
    });
  }

  openAddTextDialog(): void {
    const dialogRef = this.dialog.open(AddTextsComponent, {
      data: this.texts,
    });
    dialogRef.afterClosed().subscribe((text) => {
      if (text !== undefined) {
        this.sendText.emit(text);
      }
    });
  }
}
