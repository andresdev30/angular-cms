import { Component } from '@angular/core';
import { IconTitle } from './components/enums/IconTitle';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  userLoggedIn = 'Andrés';
  texts: string;
  isEditMode: boolean;
  showHello: boolean;
  images: string[];
  buttonIcon = IconTitle.edit;

  toggleEditMode(): void {
    this.isEditMode = !this.isEditMode;
    this.buttonIcon = this.isEditMode ? IconTitle.close : IconTitle.edit;
  }

  receivedImages(e): void {
    this.images = e;
  }

  receivedText(e): void {
    this.texts = e;
  }

  receivedShowHello(e): void {
    this.showHello = e;
  }
}
