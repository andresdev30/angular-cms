import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { QuillModule } from 'ngx-quill';

import { AddImagesComponent } from './components/edit-page/add-widget/add-images/add-images.component';
import { AddTextsComponent } from './components/edit-page/add-widget/add-texts/add-texts.component';
import { AddSayHelloComponent } from './components/edit-page/add-widget/add-say-hello/add-say-hello.component';
import { EditPageComponent } from './components/edit-page/edit-page.component';
import { PreviewPageComponent } from './components/preview-page/preview-page.component';
import { AddWidgetComponent } from './components/edit-page/add-widget/add-widget.component';
import { SayHelloComponent } from './components/preview-page/say-hello/say-hello.component';
import { ImagesComponent } from './components/preview-page/images/images.component';
import { TextsComponent } from './components/preview-page/texts/texts.component';

@NgModule({
  declarations: [
    AppComponent,
    AddImagesComponent,
    AddTextsComponent,
    AddSayHelloComponent,
    EditPageComponent,
    PreviewPageComponent,
    AddWidgetComponent,
    SayHelloComponent,
    ImagesComponent,
    TextsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    QuillModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
